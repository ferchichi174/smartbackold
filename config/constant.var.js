exports.userRole = {
    ROOT: 'root',
    ADM: 'admin',
    CLIENT: 'client',
    CONDIDAT: 'condidat'
}

exports.userState = {
    ACTIVE: 'active',
    DISABLE: 'disable',
    INVALID: 'invalid',
    BLOCKD: 'blocked',
    PENDING: 'pending' ,
    UNCOMPLETED: 'uncompleted' ,
}


exports.missionState = {
    EN_ATTENTE: 'en_attente',
    EN_COURS: 'en_cours',
    TERMINE: 'termine',
}

exports.dmdState = {
    EN_ATTENTE: 'en_attente',
    EN_COURS: 'en_cours',
    FERME_SUCCESS: 'ferme_success',
    FERME_FAIL: 'ferme_fail',
}


exports.base_url = 'http://smartbridge.fr/' ;

