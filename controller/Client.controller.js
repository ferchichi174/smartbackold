const db = require("../model");
const gst_client = db.Client;
const gst_user = db.User;
const gst_cond = db.Condidat;
const gst_dmd = db.Demande_condidat;
const gst_offre = db.Offre;
const constant = require('../config/constant.var');
const jwt = require('jsonwebtoken');
const jwt_config = require("../config/constant.config");
const mailer = require('./Mail.controller');



exports.newClientInfo = (req, res) => {

    const client = {
        type_client: req.body.type_client,
        nom_entreprise: req.body.nom_ent,
        date_creation: req.body.date_creation,
        resume: req.body.resume,
        secteur: req.body.secteur,
        site_web: req.body.site_web,
        userId: req.user.id

    };



    const id_user = req.user.id;
    const nom = req.body.nom;
    const prenom = req.body.prenom;
    const tel = req.body.tel;

    gst_client.create(client).then(data => {


        gst_user.findOne({ where: { id: id_user } }).then(user => {

            if (!user) {
                res.status(401).json({
                    message: 'user incorrect !',
                });
            } else {
                user.profile_done = true;
                user.etat = constant.userState.PENDING;
                user.nom = nom;
                user.prenom = prenom;
                user.tel = tel;

                user.save().then(rslt => {


                    const notif = {
                        userId: null ,
                        content: 'un nouveau <b>client</b> est inscrit .',
                        url: '/dashb/acc/cl',
                        read: false,
                        titre: 'new_cl'
                    }
                    db.Notif.create(notif).then(data_norif => {

                        io.to("staff_room").emit("notif", 'new notif')
                        res.send({ done: true, data: data })

                    })
                        .catch(err => {
                            res.status(500).send({
                                message: err.message || "Some error occurred while creating the notif"
                            });
                        });



                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while update colis"
                    });
                });

            }
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while try to login"
            });
        });
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the user"
            });
        });
};

exports.getClientByUser = (req, res) => {

    const id_user = req.body.id;

    gst_client.findOne({ where: { userId: id_user }, include: [{ model: db.User, attributes: ['id', 'email', 'role', 'nom', 'prenom', 'tel', 'pay', 'adresse', 'img'] }] }).then(data => {

        if (!data) {
            res.status(401).json({
                message: 'aucun client pour cette utilisateur !',
            });
        } else {
            res.send(data);

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });

};

exports.searchMatch = (req, res) => {

    let skills = req.body.skills;
    const niv_exp = req.body.niv_exp;
    const date_dispo = req.body.date_dispo;
    const tele = req.body.tele;

    //skills = skills.map(el => el.replace(`'`, `\\'`) )
    if (skills.length == 0)
        res.send([]);

    let atr_niv_exp_score = 0;

    let atr_date_dispo_score = 0;

    if (niv_exp.length)
        atr_niv_exp_score = "( condidat.niv_exp REGEXP '" + niv_exp.join('').toLowerCase() + "')";
        resu=  atr_niv_exp_score.split("|");

    if (date_dispo.length)
        atr_date_dispo_score = "( date(condidat.dispo) <= '" + date_dispo[0] + "')";

    const atr_skills_score = "((LOWER(condidat.skills) LIKE '%" + skills.join("%') +(LOWER(condidat.skills) LIKE '%").toLowerCase() + "%'))";
    const atr_skills_tot = "(" + atr_niv_exp_score + " + " + atr_skills_score + " + " + atr_date_dispo_score + " )";
    const atr_skills_where = "((LOWER(condidat.skills) LIKE '%" + skills.join("%') OR (LOWER(condidat.skills) LIKE '%").toLowerCase() + "%'))";


    if (tele == '' ){
        console.log(resu)
        gst_cond.findAll(
            {
                include: [db.Formation, db.Experience, { model: db.User, attributes: ['img', 'etat','tele'], where: { etat: 'active'} }, db.Demande_condidat],
                attributes: [
                    'id', 'date_naissance', 'sex', 'resume', 'skills', 'niv_etude', 'niv_exp', 'titre', 'dispo','tjm',
                    [db.Sequelize.literal(atr_niv_exp_score), 'c1'],
                    [db.Sequelize.literal(atr_skills_score), 'c2'],
                    [db.Sequelize.literal(atr_date_dispo_score), 'c3'],
                    [db.Sequelize.literal(atr_skills_tot), 'tsc'],

                ],
                where: {
                    [db.Sequelize.Op]: db.Sequelize.literal(atr_skills_where),niv_exp:resu

                },
                order: [db.Sequelize.literal('tsc DESC')]

            }).then(data => {

            if (!data) {
                res.status(204).json({
                    message: 'aucun client pour cette utilisateur !',
                });
            } else {
                res.send(data);

            }
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while try to login"
            });
        });

    }
    else {
        gst_cond.findAll(
            {
                include: [db.Formation, db.Experience, { model: db.User, attributes: ['img', 'etat','tele'], where: { etat: 'active',tele: tele } }, db.Demande_condidat],
                attributes: [
                    'id', 'date_naissance', 'sex', 'resume', 'skills', 'niv_etude', 'niv_exp', 'titre', 'dispo','tjm',
                    [db.Sequelize.literal(atr_niv_exp_score), 'c1'],
                    [db.Sequelize.literal(atr_skills_score), 'c2'],
                    [db.Sequelize.literal(atr_date_dispo_score), 'c3'],
                    [db.Sequelize.literal(atr_skills_tot), 'tsc'],

                ],
                where: {
                    [db.Sequelize.Op]: db.Sequelize.literal(atr_skills_where),niv_exp:resu

                },
                order: [db.Sequelize.literal('tsc DESC')]

            }).then(data => {

            if (!data) {
                res.status(204).json({
                    message: 'aucun client pour cette utilisateur !',
                });
            } else {
                res.send(data);

            }
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while try to login"
            });
        });
    }
};

exports.newDmdCond = (req, res) => {

    const dmd = {
        condidatId: req.body.condidatId,
        clientId: req.body.clientId,
        date_debut: req.body.date_debut,
        date_fin: req.body.date_fin,
        etat: 'en_attente'

    };

    gst_dmd.create(dmd).then(data => {
        res.send({ done: true, data: data })
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while update colis"
            });
        });
};


exports.createOffre = (req, res) => {

    const offre = {
        titre: req.body.titre,
        description: req.body.description,
        tech: req.body.tech,
        budget: req.body.budget,
        date_debut: req.body.date_debut,
        date_fin: req.body.date_fin,
        etat: 'ouvert',
        clientId: req.body.client_id

    };

    gst_offre.create(offre).then(data => {

        const notif = {
            userId: null ,
            content: 'une nouvelle offre d\'emploi <b>'+data.titre+'</b> à été crée .',
            url: '/dashb/offre/'+data.id,
            read: false,
            titre: 'new_ofr'
        }
        db.Notif.create(notif).then(data_norif => {

            io.to("staff_room").emit("notif", 'new notif')
            res.send({ done: true, data: data })

        })
            .catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while creating the notif"
                });
            });


    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create offre"
            });
        });
};

const getPagination = (page, size) => {
    const limit = size ? +size : 3;
    const offset = page ? page * limit : 0;

    return { limit, offset };
};

const getPagingData = (data, page, limit) => {
    const { count: totalItems, rows: offres } = data;
    const currentPage = page ? +page : 0;
    const totalPages = Math.ceil(totalItems / limit);

    return { totalItems, offres, totalPages, currentPage };
};

exports.getMyOffre = (req, res) => {

    const id_user = req.body.id;
    let page = req.body.page;
    let size = req.body.size;
    const { limit, offset } = getPagination(page, size);

    gst_offre.findAndCountAll({ where: { clientId: id_user }, limit: limit, offset: offset, include: [db.Demande_offre], order: [['id', 'DESC']] }).then(data => {
        const response = getPagingData(data, page, limit);
        res.send(response)
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create offre"
            });
        });
};

exports.searchMyOffre = (req, res) => {

    const id_user = req.body.id;
    const search = req.body.search;
    let page = req.body.page;
    let size = req.body.size;
    const { limit, offset } = getPagination(page, size);

    gst_offre.findAndCountAll({ where: { clientId: id_user, titre: { [db.Sequelize.Op.like]: `%${search}%` } }, limit: limit, offset: offset, include: [db.Demande_offre], order: [['id', 'DESC']] }).then(data => {
        const response = getPagingData(data, page, limit);
        res.send(response)
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create offre"
            });
        });
};

exports.updateOffre = (req, res) => {

    const offre = {
        id: req.body.id,
        titre: req.body.titre,
        description: req.body.description,
        tech: req.body.tech,
        budget: req.body.budget,
        date_debut: req.body.date_debut,
        date_fin: req.body.date_fin,
    };

    gst_offre.findByPk(offre.id).then(data => {

        if (!data) {
            res.status(204).send('Offre introuvable !')
        } else {
            // data = {
            //     ...data,
            //     titre: offre.titre,
            //     description: offre.description,
            //     tech: offre.tech,
            //     budget: offre.budget,
            //     date_debut: offre.date_debut,
            //     date_fin: offre.date_fin
            // }

            data.titre = offre.titre;
            data.description = offre.description;
            data.tech = offre.tech;
            data.budget = offre.budget;
            data.date_debut = offre.date_debut;
            data.date_fin = offre.date_fin;

            data.save().then(data_saved => {
                res.send({ done: true, data: data_saved })
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while Edit offre"
                });
            });
        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while finding offre"
            });
        });
};

exports.deleteOffre = (req, res) => {

    const id = req.body.id;

    gst_offre.findByPk(id).then(data => {

        if (!data) {
            res.status(204).send('Offre introuvable !')
        } else {

            data.destroy().then(data_del => {
                res.send({ done: true, data: data_del })
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while delete offre"
                });
            });
        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while delete offre"
            });
        });
};

exports.getOffreInfo = (req, res) => {

    const id = req.body.id;
    const client_id = req.body.client_id;

    gst_offre.findOne({ where: { id: id, clientId: client_id }, include: [{ model: db.Demande_offre, include: [{ model: db.Condidat, include: [db.Formation, db.Experience, db.Demande_condidat, { model: db.User, attributes: ['img'] }] }] }] }).then(data => {
        res.send(data)
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create offre"
            });
        });
};

exports.getClientProfile = (req, res) => {

    const id = req.body.id;

    gst_client.findOne({ where: { id: id }, include: [{ model: db.User, attributes: ['email', 'nom', 'prenom', 'img'] }] }).then(data => {
        res.send(data)
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create offre"
            });
        });
};

exports.updateClientProfile = (req, res) => {

    const id = req.body.id;
    const nom = req.body.nom;
    const prenom = req.body.prenom;

    const client = {
        type_client: req.body.type_client,
        nom_entreprise: req.body.nom_ent,
        date_creation: req.body.date_creation,
        resume: req.body.resume,
        secteur: req.body.secteur,
        site_web: req.body.site_web,

    };

    gst_client.findOne({ where: { id: id }, include: [db.User] }).then(data => {
        if (!data) {
            res.status(401).json({
                message: 'client introuvable !',
            });
        } else {
            data.type_client = client.type_client;
            data.nom_entreprise = client.nom_entreprise;
            data.date_creation = client.date_creation;
            data.resume = client.resume;
            data.secteur = client.secteur;
            data.site_web = client.site_web;

            if (data.user) {
                data.user.nom = nom;
                data.user.prenom = prenom
            }

            data.save().then(updt_client => {
                data.user.save().then(updt_client2 => {

                    res.send({ done: true, client: updt_client });

                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while update client"
                    });
                });
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while update client"
                });
            });

        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while update client"
            });
        });
};


exports.getClientOverview = (req, res) => {

    const id = req.body.id;
    const id_user = req.user.id;

    gst_offre.count({ where: { clientId: id } }).then(c_offre => {


        db.Message.count({ where: { id_to: id_user, read: false } }).then(c_msg => {

            gst_dmd.count({ where: { clientId: id } }).then(c_dmd => {

                db.Mission.count({ where: { clientId: id } }).then(c_mission => {

                    res.send({ nbr_offre: c_offre, nbr_msg: c_msg, nbr_dmd: c_dmd, nbr_mission: c_mission })



                })
                    .catch(err => {
                        res.status(500).send({
                            message: err.message || "Some error occurred while get overview"
                        });
                    });
            })
                .catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while get overview"
                    });
                });



        })
            .catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while get overview"
                });
            });


    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while get overview"
            });
        });
};

exports.getMissions = (req, res) => {

    const id = req.body.id;

    db.Mission.findAll({ where: { clientId: id }, include: [{ model: db.Condidat, include: 'user' }], order: [['id', 'DESC']] }).then(data => {
        res.send(data)

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred !"
            });
        });
};

exports.createFavoris = (req, res) => {

    const id_client = req.body.id_client;
    const id_cond = req.body.id_cond;

    db.Favori.create({ clientId: id_client, condidatId: id_cond }).then(data => {
        res.send({ done: true, data: data })
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create favoris"
            });
        });
};

exports.deleteFavoris = (req, res) => {

    const id = req.body.id;

    db.Favori.findByPk(id).then(data => {

        if (!data) {
            res.status(204).send('Offre introuvable !')
        } else {

            data.destroy().then(data_del => {
                res.send({ done: true, data: data_del })
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while delete favoris"
                });
            });
        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while delete favoris"
            });
        });
};

exports.getFavorisRefs = (req, res) => {

    const id = req.body.id;

    db.Favori.findAll({ where: { clientId: id }, order: [['id', 'DESC']] }).then(data => {
        res.send(data)

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred !"
            });
        });
};

exports.getFavoris = (req, res) => {

    const id = req.body.id;

    db.Favori.findAll({
        where: { clientId: id },
        include: [
            {
                model: db.Condidat,
                include: [db.Formation, db.Experience, { model: db.User, attributes: ['img', 'etat'] }, db.Demande_condidat],
                attributes: [
                    'id', 'date_naissance', 'sex', 'resume', 'skills', 'niv_etude', 'niv_exp', 'titre', 'dispo']
            }
        ]

        , order: [['id', 'DESC']]
    }).then(data => {
        res.send(data)

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred !"
            });
        });
};

exports.getMyNotif = (req, res) => {

    const id = req.user.id;

    db.Notif.findAll({
        where: { userId : id },
        order: [['id', 'DESC']]
    }).then(data => {
        res.send(data)

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred !"
            });
        });
};

exports.setNotifRead = (req, res) => {

    const id = req.user.id;

    db.Notif.update(
        {read : true },
        {where: { userId : id , read : false } }

    ).then(data => {
        res.send(data)

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred !"
            });
        });
};

