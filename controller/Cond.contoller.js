const db = require("../model");
const gst_cond = db.Condidat;
const gst_user = db.User;
const gst_dmd_offre = db.Demande_offre;
const gst_offre = db.Offre;
const constant = require('../config/constant.var');
const jwt = require('jsonwebtoken');
const jwt_config = require("../config/constant.config");
const mailer = require('./Mail.controller');



exports.newCondInfo = (req, res) => {

    const cond = {
        date_naissance: req.body.date_naiss,
        sex: req.body.sex,
        resume: req.body.resume,
        skills: req.body.skills,
        niv_etude: req.body.niv_etude,
        niv_exp: req.body.niv_exp,
        titre: req.body.titre,
        dispo: req.body.dispo,
        userId: req.user.id

    };

    const id_user = req.user.id;
    const nom = req.body.nom;
    const prenom = req.body.prenom;
    const tel = req.body.tel;

    gst_cond.create(cond).then(data => {


        gst_user.findOne({ where: { id: id_user } }).then(user => {

            if (!user) {
                res.status(401).json({
                    message: 'user incorrect !',
                });
            } else {
                user.profile_done = true;
                user.etat = constant.userState.UNCOMPLETED;
                user.nom = nom;
                user.prenom = prenom;
                user.tel = tel ;
                user.save().then(rslt => {


                    const notif = {
                        userId: null,
                        content: 'un nouveau <b>condidat</b> est inscrit .',
                        url: '/dashb/acc/cd',
                        read: false,
                        titre: 'new_cd'
                    }
                    db.Notif.create(notif).then(data_norif => {

                        io.to("staff_room").emit("notif", 'new notif')
                        res.send({ done: true, data: data })

                    })
                        .catch(err => {
                            res.status(500).send({
                                message: err.message || "Some error occurred while creating the notif"
                            });
                        });



                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while update colis"
                    });
                });

            }
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while try to login"
            });
        });
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the user"
            });
        });
};

exports.getCondtByUser = (req, res) => {

    const id_user = req.body.id;

    gst_cond.findOne({ where: { userId: id_user }, include: [db.Formation, db.Experience, { model: db.User, attributes: ['id', 'email', 'role', 'nom', 'prenom', 'tel', 'pay', 'adresse', 'img' , 'cv'] }] }).then(data => {

        if (!data) {
            res.status(401).json({
                message: 'aucun client pour cette utilisateur !',
            });
        } else {
            res.send(data);

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });

};

exports.setCondFnE = (req, res) => {


    const id_user = req.user.id;

    const lst_frm = req.body.frm;
    const lst_exp = req.body.exp;


    gst_cond.findOne({ where: { userId: id_user } }).then(cond => {

        if (!cond) {
            res.status(401).json({
                message: 'condidat incorrect !',
            });
        } else {
            const lst_frm_fnl = lst_frm.map(el => ({ ...el, condidatId: cond.id }));
            const lst_exp_fnl = lst_exp.map(el => ({ ...el, condidatId: cond.id }));

            db.Formation.bulkCreate(lst_frm_fnl, { validate: true, individualHooks: true }).then(data_frm => {

                db.Experience.bulkCreate(lst_exp_fnl, { validate: true, individualHooks: true }).then(data_exp => {

                    cond.profile_done = true;
                    cond.save().then(cond_updt => {

                        res.send({ done: true, formation: data_frm, experience: data_exp })

                    }).catch(err => {
                        res.status(500).send({
                            message: err.message || "Some error occurred while update condidat status"
                        });
                    });



                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while create experience(s)"
                    });
                });

            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while create formation(s)"
                });
            });

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try fetching condidat"
        });
    });

};

exports.setCondFormations = (req, res) => {


    const id_user = req.user.id;
    const lst_frm = req.body.frm;


    gst_cond.findOne({ where: { userId: id_user } }).then(cond => {

        if (!cond) {
            res.status(401).json({
                message: 'condidat incorrect !',
            });
        } else {
            const lst_frm_fnl = lst_frm.map(el => ({ ...el, condidatId: cond.id }));

            db.Formation.bulkCreate(lst_frm_fnl, { validate: true, individualHooks: true }).then(data_frm => {
                gst_user.findOne({ where: { id: id_user } }).then(user => {

                    user.formation_done = true;
                    user.save().then(user_updt => {

                        res.send({ done: true, formation: data_frm })

                    }).catch(err => {
                        res.status(500).send({
                            message: err.message || "Some error occurred while update condidat status"
                        });
                    });
                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while update condidat status"
                    });
                });

            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while create formation(s)"
                });
            });

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try fetching condidat"
        });
    });

};

exports.setCondExp = (req, res) => {


    const id_user = req.user.id;
    const lst_exp = req.body.exp;


    gst_cond.findOne({ where: { userId: id_user } }).then(cond => {

        if (!cond) {
            res.status(401).json({
                message: 'condidat incorrect !',
            });
        } else {
            const lst_exp_fnl = lst_exp.map(el => ({ ...el, condidatId: cond.id }));

            db.Experience.bulkCreate(lst_exp_fnl, { validate: true, individualHooks: true }).then(data_exp => {
                gst_user.findOne({ where: { id: id_user } }).then(user => {

                    user.exp_done = true;
                    user.etat = constant.userState.PENDING;
                    user.save().then(user_updt => {

                        res.send({ done: true, exp: data_exp })

                    }).catch(err => {
                        res.status(500).send({
                            message: err.message || "Some error occurred while update condidat status"
                        });
                    });
                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while update condidat status"
                    });
                });

            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while create formation(s)"
                });
            });

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try fetching condidat"
        });
    });

};


exports.getAllCond = (req, res) => {


    gst_cond.findAll({ include: [db.Formation, db.Experience, { model: db.User, attributes: ['img'] }, db.Demande_condidat] }).then(data => {

        if (!data) {
            res.status(204).json({
                message: 'aucun client pour cette utilisateur !',
            });
        } else {
            res.send(data);

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });

};


exports.searchMatch = (req, res) => {

    let skills = req.body.skills;
    const niv_exp = req.body.niv_exp;

    //skills = skills.map(el => el.replace(`'`, `\\'`) )

    const atr_niv_exp_score = "( condidat.niv_exp REGEXP '" + niv_exp.join('|').toLowerCase() + "')";
    const atr_skills_score = "((LOWER(condidat.skills) LIKE '%" + skills.join("%') +(LOWER(condidat.skills) LIKE '%").toLowerCase() + "%'))";
    const atr_skills_tot = "(" + atr_niv_exp_score + " + " + atr_skills_score + " )";
    const atr_skills_where = "((LOWER(condidat.skills) LIKE '%" + skills.join("%') OR (LOWER(condidat.skills) LIKE '%").toLowerCase() + "%'))";



    gst_cond.findAll(
        {
            include: [db.Formation, db.Experience, { model: db.User, attributes: ['img'] }, db.Demande_condidat],
            attributes: [
                'id', 'date_naissance', 'sex', 'resume', 'skills', 'niv_etude', 'niv_exp', 'titre', 'dispo',
                [db.Sequelize.literal(atr_niv_exp_score), 'c1'],
                [db.Sequelize.literal(atr_skills_score), 'c2'],
                [db.Sequelize.literal(atr_skills_tot), 'tsc'],

            ],
            where: {
                [db.Sequelize.Op]: db.Sequelize.literal(atr_skills_where)

            },
            order: [db.Sequelize.literal('tsc DESC')]

        }).then(data => {

            if (!data) {
                res.status(204).json({
                    message: 'aucun client pour cette utilisateur !',
                });
            } else {
                res.send(data);

            }
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while try to login"
            });
        });

};

exports.newDmdOffre = (req, res) => {

    const dmd = {
        condidatId: req.body.condidatId,
        offreId: req.body.offreId,

    };


    gst_dmd_offre.create(dmd).then(data => {

        gst_offre.findOne({ where: { id: dmd.offreId } }).then(offre => {


            db.Client.findOne({ where: { id: offre.clientId } }).then(client => {

                const notif = {
                    userId: client.userId,
                    content: 'Une <b>nouvelle proposition</b> pour votre offre <b>' + offre.titre + '</b>',
                    url: '/dashb/mesoffres/' + offre.id,
                    read: false,
                    titre: 'postul'
                }

                const notif_adm = {
                    userId: null,
                    content: 'Une <b>nouvelle proposition</b> pour l\'offre <b>' + offre.titre + '</b>',
                    url: '/dashb/offre/' + offre.id,
                    read: false,
                    titre: 'postul'
                }


                db.Notif.bulkCreate([notif, notif_adm]).then(data_norif => {

                    if (global.users[notif.userId])
                        global.io.to(global.users[notif.userId]).emit("notif", 'new notif')

                    io.to("staff_room").emit("notif", 'new notif')

                    res.send({ done: true, data: data })

                })
                    .catch(err => {
                        res.status(500).send({
                            message: err.message || "Some error occurred "
                        });
                    });
            })
                .catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred "
                    });
                });


        })
            .catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while update colis"
                });
            });

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while update colis"
            });
        });
};


exports.getOffreOverview = (req, res) => {


    gst_offre.findAll({ where: { etat: "ouvert" }, limit: 5 }).then(data => {
        res.send(data)
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create offre"
            });
        });
};



exports.getCondOverview = (req, res) => {

    const id = req.body.id;
    const id_user = req.user.id;


    db.Message.count({ where: { id_to: id_user, read: false } }).then(c_msg => {

        gst_dmd_offre.count({ where: { condidatId: id } }).then(c_dmd => {

            res.send({ nbr_msg: c_msg, nbr_dmd: c_dmd })



        })
            .catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while get overview"
                });
            });



    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while get overview"
            });
        });



};

exports.getCondProfile = (req, res) => {

    const id = req.body.id;

    gst_cond.findOne({ where: { id: id }, include: [db.Formation, db.Experience, { model: db.User, attributes: ['email', 'nom', 'prenom', 'img' , 'cv','tele'] }] }).then(data => {
        res.send(data)
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create offre"
            });
        });
};

const getPagination = (page, size) => {
    const limit = size ? +size : 3;
    const offset = page ? page * limit : 0;

    return { limit, offset };
};

const getPagingData = (data, page, limit) => {
    const { count: totalItems, rows: offres } = data;
    const currentPage = page ? +page : 0;
    const totalPages = Math.ceil(totalItems / limit);

    return { totalItems, offres, totalPages, currentPage };
};

exports.getNewOffres = (req, res) => {

    let page = req.body.page;
    let size = req.body.size;
    const { limit, offset } = getPagination(page, size);

    gst_offre.findAndCountAll({ where: { etat: 'ouvert' , isblocked : false  }, limit: limit, offset: offset, order: [['id', 'DESC']] }).then(data => {
        const response = getPagingData(data, page, limit);
        res.send(response)
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create offre"
            });
        });
};

exports.searchOffre = (req, res) => {

    const search = req.body.search;
    let page = req.body.page;
    let size = req.body.size;
    const { limit, offset } = getPagination(page, size);

    gst_offre.findAndCountAll({
        where: {
            [db.Sequelize.Op]: db.Sequelize.literal("(LOWER(offre.tech) LIKE '%" + search.toLowerCase() + "%' )") ,
            isblocked : false 
        },
        limit: limit, offset: offset, order: [['id', 'DESC']]
    }).then(data => {

        const response = getPagingData(data, page, limit);
        res.send(response)
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create offre"
            });
        });
};

exports.getOffreInfo = (req, res) => {

    const id = req.body.id;
    const id_cond = req.body.id_cond;

    gst_offre.findOne({ where: { id: id , isblocked : false }, include: [{ model: db.Demande_offre, where: { condidatId: id_cond }, required: false }] }).then(data => {
        res.send(data)
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while create offre"
            });
        });
};

exports.getMyNotif = (req, res) => {

    const id = req.user.id;

    db.Notif.findAll({
        where: { userId: id },
        order: [['id', 'DESC']]
    }).then(data => {
        res.send(data)

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred !"
            });
        });
};

exports.setNotifRead = (req, res) => {

    const id = req.user.id;

    db.Notif.update(
        { read: true },
        { where: { userId: id, read: false } }

    ).then(data => {
        res.send(data)

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred !"
            });
        });
};

exports.updateCondFormations = (req, res) => {


    const id_user = req.user.id;
    const lst_frm = req.body.frm;


    gst_cond.findOne({ where: { userId: id_user } }).then(cond => {

        if (!cond) {
            res.status(401).json({
                message: 'condidat incorrect !',
            });
        } else {
            const lst_frm_fnl = lst_frm.map(el => ({ ...el, condidatId: cond.id }));

            db.Formation.destroy({ where: { condidatId: cond.id } }).then(del => {
                db.Formation.bulkCreate(lst_frm_fnl, { validate: true, individualHooks: true }).then(data_frm => {


                    res.send({ done: true, formation: data_frm })


                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while update condidat formations"
                    });
                });

            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while create formation(s)"
                });
            });

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try fetching condidat"
        });
    });

};

exports.updateCondExp = (req, res) => {


    const id_user = req.user.id;
    const lst_exp = req.body.exp;


    gst_cond.findOne({ where: { userId: id_user } }).then(cond => {

        if (!cond) {
            res.status(401).json({
                message: 'condidat incorrect !',
            });
        } else {
            const lst_exp_fnl = lst_exp.map(el => ({ ...el, condidatId: cond.id }));

            db.Experience.destroy({ where: { condidatId: cond.id } }).then(del => {
                db.Experience.bulkCreate(lst_exp_fnl, { validate: true, individualHooks: true }).then(data_exp => {


                    res.send({ done: true, exp: data_exp })


                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while update condidat experiences"
                    });
                });

            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while create experiences"
                });
            });

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try fetching condidat"
        });
    });

};


exports.updateUseInfo = (req, res) => {

    const cond = {

        tele: req.body.tele,
        id: req.body.id

    };


    gst_user.findOne({ where: { id: cond.id } }).then(user => {

        if (!user) {
            res.status(401).json({
                message: 'user non valid !',
            });
        } else {
            user.tele = cond.tele;


            user.save().then(updt_condidat => {
                res.send({ done: true, data: cond.tele })

            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred "
                });
            });

        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred "
            });
        });
};

exports.updateCondInfo = (req, res) => {

    const cond = {
        date_naissance: req.body.date_naiss,
        sex: req.body.sex,
        resume: req.body.resume,
        skills: req.body.skills,
        niv_etude: req.body.niv_etude,
        niv_exp: req.body.niv_exp,
        titre: req.body.titre,
        dispo: req.body.dispo,
        id: req.body.id

    };

    const id_user = req.user.id;
    const nom = req.body.nom;
    const prenom = req.body.prenom;

    gst_cond.findOne({ where: { id: cond.id } }).then(condidat => {

        if (!condidat) {
            res.status(401).json({
                message: 'condidat non valid !',
            });
        } else {
            condidat.sex = cond.sex;
            condidat.date_naissance = cond.date_naissance;
            condidat.dispo = cond.dispo;
            condidat.niv_etude = cond.niv_etude;
            condidat.niv_exp = cond.niv_exp;
            condidat.resume = cond.resume;
            condidat.titre = cond.titre;
            condidat.skills = cond.skills;

            condidat.save().then(updt_condidat => {
                gst_user.findOne({ where: { id: id_user } }).then(user => {

                    if (!user) {
                        res.status(401).json({
                            message: 'user non valid !',
                        });
                    } else {
                        user.nom = nom;
                        user.prenom = prenom;

                        user.save().then(rslt => {

                            res.send({ done: true, data: updt_condidat })

                        }).catch(err => {
                            res.status(500).send({
                                message: err.message || "Some error occurred "
                            });
                        });

                    }

                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred "
                    });
                });

            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred "
                });
            });

        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred "
            });
        });
};

exports.getMyDmdOffer = (req, res) => {

    let id = req.body.id ;

    gst_dmd_offre.findAll({ where: { condidatId : id } , include:[{ model : db.Offre , attributes : ['id' , 'titre' , 'tech' , 'date_debut' , 'date_fin' , 'etat']}] ,  order: [['id', 'DESC']] }).then(data => {
        
        res.send(data)

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred "
            });
        });
};


