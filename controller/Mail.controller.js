const nodemailer = require('nodemailer');
const { google } = require('googleapis');

const CLIENT_ID = '541168362390-gnecnjdr71ui4ep3ve5jf6pfijd28atg.apps.googleusercontent.com';
const CLEINT_SECRET = 'J_nuXGk_XNEYhCAYvXEgsgD8';
const REDIRECT_URI = 'https://developers.google.com/oauthplayground';
const REFRESH_TOKEN = '1//04B94ByBy8FoHCgYIARAAGAQSNwF-L9IrjP9Yp9R3CkSEAHZRSpgaSdUYVbbo0iYSQqmxX7-f8DZ3G1e7lG1SkmT_PClYMJjMmZA';

const oAuth2Client = new google.auth.OAuth2(
    CLIENT_ID,
    CLEINT_SECRET,
    REDIRECT_URI
  );
  oAuth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

exports.sendMail =  async function (email , link ) {
    try {
      const accessToken = await oAuth2Client.getAccessToken();
  
      const transport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          type: 'OAuth2',
          user: 'app.smartbridge@gmail.com',
          clientId: CLIENT_ID,
          clientSecret: CLEINT_SECRET,
          refreshToken: REFRESH_TOKEN,
          accessToken: accessToken,
        },
      });
  
      const mailOptions = {
        from: 'app.smartbridge@gmail.com',
        to: email,
        subject: '[Smart-Bridge] Complétez votre inscription',
        html: '<b>Bienvenue sur Smart-Bridge</b> <br>'+
              'Pour valider votre compte et veuillez compléter votre profil en cliquant sur le lien ci-dessous :<br>'+
              '<a href="'+link+'" style="text-decoration: none; color: #ffffff; font-weight: 600; padding: 20px; background-color: #0092ff; display: inline-block; border-radius: 8px;" >Confirmer mon adresse mail</a> <br>'+
              'Si vous rencontrez des problèmes avec le bouton ci-dessus, veuillez copier-coller l\'URL suivante dans votre navigateur Web.<br>'+
              '<a href="'+link+'">'+link+'</a> ',
      };
  
      const result = await transport.sendMail(mailOptions);
      return result;
    } catch (error) {
      return error;
    }
  }





exports.sendResetPassMail =  async function (email , link ) {
    try {
      const accessToken = await oAuth2Client.getAccessToken();
  
      const transport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          type: 'OAuth2',
          user: 'app.smartbridge@gmail.com',
          clientId: CLIENT_ID,
          clientSecret: CLEINT_SECRET,
          refreshToken: REFRESH_TOKEN,
          accessToken: accessToken,
        },
      });
  
      const mailOptions = {
        from: 'app.smartbridge@gmail.com',
        to: email,
        subject: '[Smart-Bridge] Réinitialiser le mot de passe',
        html: '<b>Bienvenue sur Smart-Bridge</b> <br>'+
              'Pour Réinitialiser votre mot de passe veuillez cliquer sur le lien ci-dessous :<br>'+
              '<a href="'+link+'" style="text-decoration: none; color: #ffffff; font-weight: 600; padding: 20px; background-color: #0092ff; display: inline-block; border-radius: 8px;" >Réinitialiser mon mot de passe</a> <br>'+
              'Si vous rencontrez des problèmes avec le bouton ci-dessus, veuillez copier-coller l\'URL suivante dans votre navigateur Web.<br>'+
              '<a href="'+link+'">'+link+'</a> ',
      };
  
      const result = await transport.sendMail(mailOptions);
      return result;
    } catch (error) {
      return error;
    }
  }