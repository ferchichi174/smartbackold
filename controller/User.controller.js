const db = require("../model");
const gst_user = db.User;
const constant = require('../config/constant.var');
const jwt = require('jsonwebtoken');
const jwt_config = require("../config/constant.config");
const mailer = require('./Mail.controller');


exports.registerAdm = (req, res) => {

    const user = {
        password: req.body.password,
        email: req.body.email,
        role: constant.userRole.ADM,
        nom: req.body.nom,
        prenom: req.body.prenom,
        etat: constant.userState.ACTIVE,

    };
    gst_user.count({ where: { email: req.body.email } }).then(count => {
        if (count > 0) {
            res.status(400).send({
                message: "e-mail existe déja !"
            });
        } else {
            gst_user.create(user).then(data => {
                res.send(data)
            })
                .catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while creating the user"
                    });
                });
        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while cheking the user email"
            });
        });
};

exports.registerClient = (req, res) => {

    const user = {
        password: req.body.password,
        email: req.body.email,
        role: constant.userRole.CLIENT,
        nom: req.body.nom,
        prenom: req.body.prenom,
        etat: constant.userState.INVALID,

    };

    gst_user.count({ where: { email: req.body.email } }).then(count => {
        if (count > 0) {
            res.status(400).send({
                message: "e-mail existe déja !"
            });
        } else {
            gst_user.create(user).then(data => {

                const token = jwt.sign(
                    {
                        id: data.id,
                        role: data.role,
                        etat: data.etat,
                        nom: data.nom,
                        prenom: data.prenom,
                    },
                    jwt_config.secret, {},
                );

                mailer.sendMail(data.email, constant.base_url + 'register/vfcl/' + token)
                    .then((result) => {
                        console.log('Email sent...', result)
                        res.send({ done: true, user: data, token: token })

                    })
                    .catch((error) => {
                        console.log(error.message);
                        res.status(500).send({
                            message: err.message || "Some error occurred while creating the user"
                        });
                    });


            })
                .catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while creating the user"
                    });
                });
        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while cheking the user email"
            });
        });
};

exports.registerCondidat = (req, res) => {

    const user = {
        password: req.body.password,
        email: req.body.email,
        role: constant.userRole.CONDIDAT,
        nom: req.body.nom,
        prenom: req.body.prenom,
        etat: constant.userState.INVALID,

    };
    gst_user.count({ where: { email: req.body.email } }).then(count => {
        if (count > 0) {
            res.status(400).send({
                message: "e-mail existe déja !"
            });
        } else {
            gst_user.create(user).then(data => {

                const token = jwt.sign(
                    {
                        id: data.id,
                        role: data.role,
                        etat: data.etat,
                        nom: data.nom,
                        prenom: data.prenom,
                    },
                    jwt_config.secret, {},
                );

                mailer.sendMail(data.email, constant.base_url + 'register/vfcd/' + token)
                    .then((result) => {
                        console.log('Email sent...', result)
                        res.send({ done: true, user: data, token: token })

                    })
                    .catch((error) => {
                        console.log(error.message);
                        res.status(500).send({
                            message: err.message || "Some error occurred while creating the user"
                        });
                    });


            })
                .catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while creating the user"
                    });
                });
        }

    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while cheking the user email"
            });
        });
};

exports.reSendVerifMail = (req, res) => {

    const id = req.user.id;


    gst_user.findOne({ where: { id: id } }).then(data => {

        if (!data) {
            res.status(400).send({
                message: "utilisateur non existant !"
            });

        } else {
            const token = jwt.sign(
                {
                    id: data.id,
                    role: data.role,
                    etat: data.etat,
                    nom: data.nom,
                    prenom: data.prenom,
                },
                jwt_config.secret, {},
            );

            let url = "";
            if (data.role == constant.userRole.CLIENT) {
                url = constant.base_url + 'register/vfcl/' + token
            } else if (data.role == constant.userRole.CONDIDAT) {
                url = constant.base_url + 'register/vfcd/' + token
            }

            mailer.sendMail(data.email, url)
                .then((result) => {
                    console.log('Email sent...', result)
                    res.send({ done: true, user: data, token: token })

                })
                .catch((error) => {
                    console.log(error.message);
                    res.status(500).send({
                        message: err.message || "Some error occurred while creating the user"
                    });
                });
        }


    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the user"
            });
        });

};

exports.verifMail = (req, res) => {

    const id = req.user.id;


    gst_user.findOne({ where: { id: id } }).then(data => {

        if (!data) {
            res.status(400).send({
                message: "utilisateur non existant !"
            });

        } else {

            if (data.etat == constant.userState.INVALID)
                data.etat = constant.userState.UNCOMPLETED

            data.save().then(user => {
                res.send({
                    done: true,

                    user: {
                        email: user.email,
                        nom: user.nom,
                        prenom: user.prenom,
                        role: user.role,
                        img: user.img,
                        profile_done: user.profile_done,
                        formation_done: user.formation_done,
                        exp_done: user.exp_done,
                    },
                })
            })
        }


    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the user"
            });
        });

};

exports.checkUserProfile = (req, res) => {

    const id = req.user.id;

    gst_user.findOne({ where: { id: id } }).then(user => {

        if (!user) {
            res.status(400).send({
                message: "utilisateur non existant !"
            });

        } else {
            res.send({
                user: {
                    email: user.email,
                    nom: user.nom,
                    prenom: user.prenom,
                    role: user.role,
                    img: user.img,
                    profile_done: user.profile_done,
                    formation_done: user.formation_done,
                    exp_done: user.exp_done,
                }
            })

        }


    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the user"
            });
        });

};

exports.checkCvUploaded = (req, res) => {

    const id = req.user.id;

    gst_user.findOne({ where: { id: id } }).then(user => {

        if (!user) {
            res.status(400).send({
                message: "utilisateur non existant !"
            });

        } else {
            if(user.cv)
            return res.send({done : true})
            else
            return res.send({done : false})
        }
    })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred."
            });
        });

};



exports.login = (req, res) => {

    const email = req.body.email;
    const password = req.body.password;

    gst_user.findOne({ where: { email: email }, include: [{ model: db.Client, attributes: ['id'] }, { model: db.Condidat, attributes: ['id'] }] }).then(user => {

        if (!user) {
            res.status(401).json({
                message: 'e-mail ou mot de passe incorrect !',
            });
        } else {
            if (user.etat == constant.userState.ACTIVE || user.etat == constant.userState.DISABLE || user.etat == constant.userState.PENDING || user.etat == constant.userState.UNCOMPLETED)
                user.comparePasswords(password, (error, isMatch) => {

                    if (isMatch && !error) {
                        const token = jwt.sign(
                            {
                                id: user.id,
                                role: user.role,
                                etat: user.etat,
                                nom: user.nom,
                                prenom: user.prenom,
                                client_id: user.client ? user.client.id : null,
                                cond_id: user.condidat ? user.condidat.id : null

                            },
                            jwt_config.secret, {},
                        );


                        res.header('sb-tkn', token).send({
                            success: true,
                            token: `${token}`,
                            user: {
                                email: user.email,
                                nom: user.nom,
                                prenom: user.prenom,
                                role: user.role,
                                img: user.img,
                                profile_done: user.profile_done,
                                formation_done: user.formation_done,
                                exp_done: user.exp_done,
                            },
                        });
                    } else {

                        res.status(401).json({
                            message: 'e-mail ou mot de passe incorrect !',
                        });
                    }
                });
            else if (user.etat == constant.userState.BLOCKD)
                res.status(401).json({
                    message: 'Votre compte à été blocké !',
                });
            else
                res.status(401).json({
                    message: 'Votre e-mail n\'est pas encore validé, veuillez vérifier votre boite de réception afin de valider votre compte.',
                });
        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });
};

exports.refreshToken = (req, res) => {

    const id = req.user.id;

    gst_user.findOne({ where: { id: id }, include: [{ model: db.Client, attributes: ['id'] }, { model: db.Condidat, attributes: ['id'] }] }).then(user => {

        if (!user) {
            res.status(401).json({
                message: 'e-mail ou mot de passe incorrect !',
            });
        } else {
            if (user.etat == constant.userState.ACTIVE || user.etat == constant.userState.DISABLE || user.etat == constant.userState.PENDING || user.etat == constant.userState.UNCOMPLETED) {

                const token = jwt.sign(
                    {
                        id: user.id,
                        role: user.role,
                        etat: user.etat,
                        nom: user.nom,
                        prenom: user.prenom,
                        client_id: user.client ? user.client.id : null,
                        cond_id: user.condidat ? user.condidat.id : null

                    },
                    jwt_config.secret, {},
                );


                res.header('sb-tkn', token).send({
                    success: true,
                    token: `${token}`,
                    user: {
                        email: user.email,
                        nom: user.nom,
                        prenom: user.prenom,
                        role: user.role,
                        img: user.img,
                        profile_done: user.profile_done,
                        formation_done: user.formation_done,
                        exp_done: user.exp_done,
                    },
                });


            } else if (user.etat == constant.userState.BLOCKD)
                res.status(401).json({
                    message: 'Votre compte à été blocké !',
                });
            else
                res.status(401).json({
                    message: 'Votre e-mail n\'est pas encore validé, veuillez vérifier votre boite de réception afin de valider votre compte.',
                });
        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });
};


exports.checkUser = (req, res) => {
    const user = req.user;
    if (!user) {
        res.status(401).json({
            message: 'utilisateur introuvable',
        });

    } else {
        res.send(user);
    }

};

exports.validateUser = (req, res) => {

    const id = req.body.id;

    const notif = {
        userId: id,
        content: 'Votre compte à été validé par l\'equipe Smart-Bridge .',
        url: '/dashb/',
        read: false,
        titre: 'valid_cmpt'
    }


    gst_user.findOne({ where: { id: id } }).then(user => {

        if (!user) {
            res.status(204).json({
                message: 'User not found',
            });
        } else {
            user.etat = constant.userState.ACTIVE;
            user.save().then(user_updt => {

                db.Notif.create(notif).then(data_norif => {

                    if (global.users[notif.userId])
                        global.io.to(global.users[notif.userId]).emit("notif_valid_cmpt", 'new notif')

                    res.send({ done: true })

                })
                    .catch(err => {
                        res.status(500).send({
                            message: err.message || "Some error occurred "
                        });
                    });

            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while try to validate user"
                });
            });

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });
};
exports.inValidateUser = (req, res) => {

    const id = req.body.id;


    gst_user.findOne({ where: { id: id } }).then(user => {

        if (!user) {
            res.status(204).json({
                message: 'User not found',
            });
        } else {
            user.etat = constant.userState.PENDING;
            user.save().then(user_updt => {
                res.send({ done: true })
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while try to validate user"
                });
            });

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });
};

exports.changePass = (req, res) => {

    const last_password = req.body.last_password;
    const password = req.body.password;

    gst_user.findOne({ where: { id: req.user.id } }).then(user => {
        if (!user) {
            res.status(401).json({
                message: 'utilisateur introuvable',
            });


        } else {
            user.comparePasswords(last_password, (error, isMatch) => {

                if (isMatch && !error) {
                    gst_user.update({ password: password }, { where: { id: user.id } }).then(usr => {
                        res.send({ done: true });
                    }).catch(err => {
                        res.status(500).send({

                            message: err.message || "Some error occurred while try to change password"
                        });
                    });
                } else {
                    res.status(401).json({
                        message: 'Ancien mot de passe incorrect !',
                    });
                }

            })
        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });

};

exports.forgetPass = (req, res) => {


    gst_user.findOne({ where: { email: req.body.email } }).then(data => {
        if (!data) {
            res.status(401).json({
                message: 'Aucune utilisateur trouvé avec cette adresse mail !',
            });


        } else {
            const token = jwt.sign(
                {
                    id: data.id,
                    role: data.role,
                    etat: data.etat,
                    nom: data.nom,
                    prenom: data.prenom,
                },
                jwt_config.secret, {},
            );

            mailer.sendResetPassMail(data.email, constant.base_url + 'acc/rst-pass/' + token)
                .then((result) => {
                    console.log('Email sent...', result)
                    res.send({ done: true })

                })
                .catch((error) => {
                    console.log(error.message);
                    res.status(500).send({
                        message: err.message || "Some error occurred while creating the user"
                    });
                });
        
        }
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });

};

exports.resetPass = (req, res) => {

    const password = req.body.password;

    gst_user.findOne({ where: { id: req.user.id } }).then(user => {
        if (!user) {
            res.status(401).json({
                message: 'utilisateur introuvable',
            });


        } else {
                    gst_user.update({ password: password }, { where: { id: user.id } }).then(usr => {
                        res.send({ done: true });
                    }).catch(err => {
                        res.status(500).send({

                            message: err.message || "Some error occurred while try to change password"
                        });
                    });
        
        }

    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });

};


exports.banUser = (req, res) => {

    const id = req.body.id;

    gst_user.findOne({ where: { id: id } }).then(user => {

        if (!user) {
            res.status(204).json({
                message: 'User not found',
            });
        } else {
            user.etat = constant.userState.BLOCKD;
            user.save().then(user_updt => {
                res.send({ done: true })
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while try to validate user"
                });
            });

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });
};

exports.unbanUser = (req, res) => {

    const id = req.body.id;

    gst_user.findOne({ where: { id: id } }).then(user => {

        if (!user) {
            res.status(204).json({
                message: 'User not found',
            });
        } else {
            user.etat = constant.userState.ACTIVE;
            user.save().then(user_updt => {
                res.send({ done: true })
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while try to validate user"
                });
            });

        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while try to login"
        });
    });
};