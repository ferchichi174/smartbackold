const util = require("util");
const multer = require("multer");
const maxSize = 2 * 1024 * 1024;
const path = require('path')

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, __basedir + "/uploads/" + req.dir);
  },
  filename: (req, file, cb) => {
    console.log(file.originalname);
    const timestamp = new Date().getTime();
    cb(null, `${timestamp}${file.originalname}`);
  },
});

let uploadFile = multer({
  storage: storage,
  limits: { fileSize: maxSize },
  fileFilter: function (_req, file, cb) {
    checkFileType(file, cb);
  }
}).single("file");

function checkFileType(file, cb) {
  // Allowed ext
  const filetypes = /jpeg|jpg|png|pdf|doc|docx/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  } else {
    cb('extension de fichier non autorisée !');
  }
}

let uploadFileMiddleware = util.promisify(uploadFile);
module.exports = uploadFileMiddleware;