module.exports = (sequelize, Sequelize) => {


    const condidat = sequelize.define("condidat", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
        },
        etat: {
            type: Sequelize.ENUM,
            values: ['libre', 'en_mission'],
            allowNull: false,
            defaultValue: 'libre' ,
        },
        date_naissance: {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        sex: {
            type: Sequelize.ENUM,
            values: ['male', 'female'],
            allowNull: false
        },
        resume: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        skills: {
            type: Sequelize.JSON,
            allowNull: false
        },
        niv_etude: {
            type: Sequelize.ENUM,
            values: ['secondaire', 'bac', 'bac+3', 'bac+4', 'bac+5', 'bac+8', 'autre'],
            allowNull: false
        },
        niv_exp: {
            type: Sequelize.ENUM,
            values: ['<1', '1', '2','3','4','5','6','7','8','9','10','>10'],
            allowNull: false
        },

        titre: {
            type: Sequelize.STRING,
            allowNull: false
        },

        dispo: {
            type: Sequelize.DATEONLY,
        },
        tjm: {
            type: Sequelize.DECIMAL(10, 0),
            allowNull: false,
            defaultValue: 0.0
        },

        






    });






    return condidat;
};