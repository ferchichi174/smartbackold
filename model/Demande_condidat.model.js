module.exports = (sequelize, Sequelize) => {


    const demande_condidat = sequelize.define("demande_condidat", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
        },
        date_debut: {
            type: Sequelize.DATEONLY,
            allowNull: false,
        },
        date_fin: {
            type: Sequelize.DATEONLY,
            allowNull: false,
        },
        etat: {
            type: Sequelize.ENUM,
            values: ['en_attente', 'en_cours','ferme_success','ferme_fail'],
            allowNull: false ,
        },
        observation: {
            type: Sequelize.STRING,
        },
        



    });






    return demande_condidat;
};