module.exports = (sequelize, Sequelize) => {


    const formation = sequelize.define("formation", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
        },
        nom: {
            type: Sequelize.STRING,
            allowNull: false
        },
        ecole: {
            type: Sequelize.STRING,
            allowNull: false
        },
        date_debut: {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        date_fin: {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        en_cour: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },



    });






    return formation;
};