module.exports = (sequelize, Sequelize) => {


    const message = sequelize.define("message", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
        },
        content: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        read: {
            type: Sequelize.BOOLEAN,
            allowNull: false ,
            defaultValue : false ,
        },
        id_from:{
            type:Sequelize.INTEGER
        },
        id_to:{
            type:Sequelize.INTEGER,
        },


    });


    return message;
};