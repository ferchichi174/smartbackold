module.exports = (sequelize, Sequelize) => {


    const mission = sequelize.define("mission", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
        },
        date_debut: {
            type: Sequelize.DATEONLY,
            allowNull: false,
        },
        date_fin: {
            type: Sequelize.DATEONLY,
            allowNull: false,
        },
        tjm: {
            type: Sequelize.DECIMAL(10, 0),
            allowNull: false,
            defaultValue: 0.0
        },
        etat: {
            type: Sequelize.ENUM,
            values: ['en_attente', 'en_cours','termine'],
            allowNull: false ,
        },
        



    });






    return mission;
};