module.exports = (sequelize, Sequelize) => {


    const Notif = sequelize.define("Notif", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
        },
        titre: {
            type: Sequelize.STRING,
            allowNull: false
        },
        content: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        url: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        read: {
            type: Sequelize.BOOLEAN,
            allowNull: false ,
            defaultValue : false ,
        },


    });


    return Notif;
};