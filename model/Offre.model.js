module.exports = (sequelize, Sequelize) => {


    const offre = sequelize.define("offre", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
        },
        titre: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        tech: {
            type: Sequelize.JSON,
            allowNull: false
        },
        budget: {
            type: Sequelize.DECIMAL(10, 0),
            allowNull: false,
            defaultValue: 0.0
        },
        date_debut: {
            type: Sequelize.DATEONLY,
            allowNull: false,
        },
        date_fin: {
            type: Sequelize.DATEONLY,
            allowNull: false,
        },
        etat: {
            type: Sequelize.ENUM,
            values: ['ouvert', 'en_cours', 'ferme'],
            allowNull: false,
        },
        isblocked: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
        }




    });






    return offre;
};