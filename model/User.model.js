var bcrypt = require('bcryptjs');
module.exports = (sequelize, Sequelize) => {


  const User = sequelize.define("user", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
    },
    ref: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      unique: true,
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {
          msg: "email invalide"
        }
      }
    },
    role: {
      type: Sequelize.ENUM,
      values: ['root', 'admin', 'client', 'condidat'],
      allowNull: false
    },
    etat: {
      type: Sequelize.ENUM,
      values: ['invalid', 'uncompleted', 'pending', 'active', 'disable', 'blocked'],
      allowNull: false
    },
    nom: {
      type: Sequelize.STRING,
    },
    prenom: {
      type: Sequelize.STRING,
    },
    tel: {
      type: Sequelize.STRING,
    },
    pay: {
      type: Sequelize.STRING,
    },
    adresse: {
      type: Sequelize.TEXT
    },
    img: {
      type: Sequelize.TEXT
    },
    profile_done: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    formation_done: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    exp_done: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    cv: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    tele: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },



  }, {

    hooks: {
      beforeValidate: hashPassword
    },
  });



  // Compare passwords
  User.prototype.comparePasswords = function (password, callback) {
    bcrypt.compare(password, this.password, function (error, isMatch) {
      if (error) {
        return callback(error);
      }

      return callback(null, isMatch);
    });
  }

  // Hash the password
  function hashPassword(user) {
    console.log("from model ");
    console.log(user.password);
    if (user.changed('password')) {
      return bcrypt.hash(user.password, 10).then(function (password) {
        user.password = password;
      });
    }
  }



  return User;
};