const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//models
db.User = require("./User.model.js")(sequelize, Sequelize);
db.Condidat = require("./Condidat.model.js")(sequelize, Sequelize);
db.Client = require("./Client.model.js")(sequelize, Sequelize);
db.Formation = require("./Formation.model.js")(sequelize, Sequelize);
db.Experience = require("./Experience.model.js")(sequelize, Sequelize);
db.Demande_condidat = require("./Demande_condidat.model.js")(sequelize, Sequelize);
db.Offre = require("./Offre.model.js")(sequelize, Sequelize);
db.Message = require("./Message.model.js")(sequelize, Sequelize);
db.Notif = require("./Notif.model.js")(sequelize, Sequelize);
db.Demande_offre = require("./Demande_offre.model")(sequelize, Sequelize);
db.Mission = require("./Mission.model")(sequelize, Sequelize);
db.Favori = require("./Favori.model")(sequelize, Sequelize);
db.Contactus = require("./Contactus.model")(sequelize, Sequelize);

db.User.hasOne(db.Condidat , {onUpdate:'CASCADE',onDelete:'CASCADE' , foreignKey: {
  unique: true
}});
db.Condidat.belongsTo(db.User);
db.User.hasOne(db.Client , {onUpdate:'CASCADE',onDelete:'CASCADE' , foreignKey: {
  unique: true
}});
db.Client.belongsTo(db.User);

db.Condidat.hasMany(db.Formation ,{onUpdate:'CASCADE',onDelete:'CASCADE'});
db.Formation.belongsTo(db.Condidat,{onUpdate:'CASCADE' , onDelete:'CASCADE'});

db.Condidat.hasMany(db.Experience ,{onUpdate:'CASCADE',onDelete:'CASCADE'});
db.Experience.belongsTo(db.Condidat,{onUpdate:'CASCADE' , onDelete:'CASCADE'});

db.Client.hasMany(db.Demande_condidat ,{onUpdate:'CASCADE',onDelete:'CASCADE'});
db.Demande_condidat.belongsTo(db.Client,{onUpdate:'CASCADE' , onDelete:'CASCADE'});

db.Condidat.hasMany(db.Demande_condidat ,{onUpdate:'CASCADE',onDelete:'CASCADE'});
db.Demande_condidat.belongsTo(db.Condidat,{onUpdate:'CASCADE' , onDelete:'CASCADE'});

db.Client.hasMany(db.Offre ,{onUpdate:'CASCADE',onDelete:'CASCADE'});
db.Offre.belongsTo(db.Client,{onUpdate:'CASCADE' , onDelete:'CASCADE'});

db.User.hasMany(db.Message ,{as:'userMsgFrom',foreignKey:'id_from',sourceKey:'id'  ,onUpdate: 'CASCADE', onDelete: 'CASCADE'});
db.Message.belongsTo(db.User ,{as:'msgFrom',foreignKey:'id_from',targetKey:'id'  ,onUpdate: 'CASCADE', onDelete: 'CASCADE'});

db.User.hasMany(db.Message ,{as:'userMsgTo',foreignKey:'id_to',sourceKey:'id'  ,onUpdate: 'CASCADE', onDelete: 'CASCADE'});
db.Message.belongsTo(db.User ,{as:'msgTo',foreignKey:'id_to',targetKey:'id'  ,onUpdate: 'CASCADE', onDelete: 'CASCADE'});

db.User.hasMany(db.Notif ,{onUpdate: 'CASCADE', onDelete: 'CASCADE'});
db.Notif.belongsTo(db.User ,{onUpdate: 'CASCADE', onDelete: 'CASCADE'});

db.Offre.hasMany(db.Demande_offre ,{onUpdate:'CASCADE',onDelete:'CASCADE'});
db.Demande_offre.belongsTo(db.Offre,{onUpdate:'CASCADE' , onDelete:'CASCADE'});

db.Condidat.hasMany(db.Demande_offre ,{onUpdate:'CASCADE',onDelete:'CASCADE'});
db.Demande_offre.belongsTo(db.Condidat,{onUpdate:'CASCADE' , onDelete:'CASCADE'});


db.Client.hasMany(db.Mission ,{onUpdate:'CASCADE',onDelete:'CASCADE'});
db.Mission.belongsTo(db.Client,{onUpdate:'CASCADE' , onDelete:'CASCADE'});

db.Condidat.hasMany(db.Mission ,{onUpdate:'CASCADE',onDelete:'CASCADE'});
db.Mission.belongsTo(db.Condidat,{onUpdate:'CASCADE' , onDelete:'CASCADE'});

db.Client.hasMany(db.Favori ,{onUpdate:'CASCADE',onDelete:'CASCADE'});
db.Favori.belongsTo(db.Client,{onUpdate:'CASCADE' , onDelete:'CASCADE'});

db.Condidat.hasMany(db.Favori ,{onUpdate:'CASCADE',onDelete:'CASCADE'});
db.Favori.belongsTo(db.Condidat,{onUpdate:'CASCADE' , onDelete:'CASCADE'});








module.exports = db;