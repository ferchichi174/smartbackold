module.exports = app => {
    const rep = require("../controller/Message.controller");
    const rep_adm = require("../controller/Adm.controller");
    const verify = require("../midleware/TokenCheck");
    var router = require("express").Router();

   
    router.post("/sendit" ,verify.verifyAll , rep.newMsg);
    router.post("/getMyMsg" ,verify.verifyAll , rep.getMyMsg);
    router.post("/getMsgByUser" ,verify.verifyAdmin , rep.getMsgByUser);
    router.post("/getUserChat" ,verify.verifyAdmin , rep_adm.getUserChat);



    app.use('/api/chat',router);
};