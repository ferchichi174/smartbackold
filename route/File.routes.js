module.exports = app => {
    const service = require("../controller/File.controller");
    var router = require("express").Router();
    const verify = require("../midleware/TokenCheck");

    router.post("/upload",  verify.verifyAll , service.upload);
    router.post("/upload/cv",  verify.verifyAll , service.upload_cv);
    router.get("/download/cv/:name", service.download_cv);
    router.get("/download/:name", service.download);
    router.get("/get", verify.verifyAdmin , service.getListFiles);
    

    app.use('/api/',router);
};