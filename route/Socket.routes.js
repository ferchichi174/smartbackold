
module.exports = (io , users , users_adm) => {


   const jwt = require('jsonwebtoken');
  const jwt_config = require("../config/constant.config");
  // var users = {};
  // var users_adm = {};

  io.use((socket, next) => {
    console.log(`midleware token  ${socket.handshake.auth.token} `)
    const user = verify(socket.handshake.auth.token);
    //console.log(user)
    if (user) {
      console.log('ok')
      socket.user = user;
      next()
    } else {
      console.log('failed')
      next(new Error("invalid token"))
    }


  });

  io.on('connection', (socket) => {
    users[socket.user.id] = socket.id;

    if (socket.user.role == "admin") {
      users_adm[socket.user.id] = socket.id;
      socket.join("staff_room")
    }

    console.log(`Connecté au client ${socket.id} - ${socket.user}`)
    console.log(users)
    console.log(io.engine.clientsCount)
    // émission d'un évènement
    io.emit('connected_lst', users)

    socket.on('disconnect', function () {
      delete users[socket.user.id];
      console.log(`diconnected au client ${socket.id} - ${socket.user}`)
      console.log(users)

      console.log(io.engine.clientsCount)
      io.emit('connected_lst', users)

    });

    socket.on('get_connected_lst', function () {

      console.log(`call for connected users ${socket.id} - ${socket.user}`)
      io.to(socket.id).emit("connected_lst", users)

    });

    socket.on('client_msg', function (msg) {
      console.log("new  msg "+msg)
      io.to("staff_room").emit("new_msg", {msg :msg , user : socket.user.id})

    });

    socket.on('notify_client_msg', function (data) {
      console.log("notify client "+JSON.stringify(data))

      if(users[data.user])
      io.to(users[data.user]).emit("new_msg_to_client", data.data)

    });

  })



  function verify(token) {
    if (!token)
      return null;

    try {
      const verified = jwt.verify(token, jwt_config.secret);
      return verified;

    } catch (error) {
      return null;
    }
  }



};
