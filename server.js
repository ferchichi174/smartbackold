const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();

global.__basedir = __dirname;

var corsOptions = {
  origin: "http://localhost:3000"
};

const db = require("./model");
/*
db.Client.destroy({ where: { nom_entreprise: ["SMART BRIDGE" ] } }).then( u => {
console.log(u)
})
*/

app.use(cors());

//app.use(bodyParser());
// parse requests of content-type - application/json
//app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
//app.use(bodyParser.urlencoded({ parameterLimit: 100000 , extended: true , limit: '50mb'}));

app.use(bodyParser.json({ limit: '50mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))

// simple route
app.get("/", (req, res) => {
  res.json({ message: "smart-bridge REST API." });
});

require("./route/File.routes")(app);
require("./route/User.routes")(app);
require("./route/Client.routes")(app);
require("./route/Cond.routes")(app);
require("./route/Adm.routes")(app);
require("./route/Chat.routes")(app);





// const data = require('./data_set.js')
/*
 db.sequelize.sync({ alter : true}).then(() => {
   console.log("db sync alter done.")
 })
 */

// });

// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync db.");
//     db.User.create({
//         password: "123456",
//         email: "admin@gmail.com",
//         role: "admin",
//         etat: "active"});

//     db.User.create({
//         password: "123456",
//         email: "client1@gmail.com",
//         role: "client",
//         etat: "invalid"});
//     db.User.create({
//         password: "123456",
//         email: "cond1@gmail.com",
//         role: "condidat",
//         etat: "invalid"});

// });




// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync db.");
//     db.User.create({
//         password: "123456",
//         email: "admin@gmail.com",
//         role: "admin",
//         etat: "active"});

//     db.User.create({
//         password: "123456",
//         email: "client1@gmail.com",
//         role: "client",
//         etat: "invalid"});
//     db.User.create({
//         password: "123456",
//         email: "cond1@gmail.com",
//         role: "condidat",
//         etat: "invalid"});

// });


app.get("/rtrt", (req, res) => {
  const id = req.query.id;
    io.to(id).emit("notif","rest provoc socket for this usr")
    res.send();
});

// set port, listen for requests
const PORT = process.env.PORT || 5000;
const server = app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

//io on top off express server

global.users = {}
global.users_adm = {};
global.io = require('socket.io')(server, {
  cors: {
    origin: '*',
  }
});

require("./route/Socket.routes")(global.io , global.users , global.users_adm);











// const PORT_RT = process.env.PORT || 5001;
// appRT.listen(PORT_RT, () => {
//     console.log(`Server is running on port ${PORT_RT}.`);
//   });
